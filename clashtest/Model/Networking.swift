
import Foundation
import UIKit
import Alamofire
//import AlamofireImage

typealias ServiceSuccessResponse = (Int, AnyObject?) -> Void
typealias ServiceErrorResponse = (Int, Error?, AnyObject?) -> Void
typealias CardsJSON = [String: Any]

class Networking {
    
    //Singleton
    static let sharedInstance = Networking()
    
    private init() {
    }
    
    
    /* Endpoints */
    func getCards(onSuccess: @escaping ServiceSuccessResponse, onError: @escaping ServiceErrorResponse) {
        genericRequest(parameters: nil, type: .get, route: "api/cards", onSuccess: onSuccess, onError: onError)
    }
    
    func getImage(ImageName : String, onSuccess: @escaping ServiceSuccessResponse, onError: @escaping ServiceErrorResponse) {
        genericRequest(parameters: nil, type: .delete, route: "images/cards/\(ImageName)", onSuccess: onSuccess, onError: onError)
    }
    
    
    /* Generic request */
    private func genericRequest(parameters : [String : Any]?, type: HTTPMethod, route: String,  onSuccess: @escaping ServiceSuccessResponse, onError: @escaping ServiceErrorResponse) {
        
        let url = K.networking.baseURL + route
        let headers =  ["Content-Type": "application/json"];
        
        Alamofire.request(url, method: type, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            self.handlerResult(response: response, onSuccess:onSuccess, onError: onError);
        }
    }
    
    
    
    /* Handle */
    func handlerExternalResult(response : DataResponse<Any>, onSuccess: ServiceSuccessResponse, onError: ServiceErrorResponse) {
        
        let result = response.result;
        let error = result.error;
        let value = result.value;
        
        if error != nil || response.response == nil {
            onError(response.response != nil ? response.response!.statusCode : 0, error, nil)
        } else {
            onSuccess(response.response!.statusCode, value as AnyObject?)
        }
        
        
        if error != nil || !response.result.isSuccess {
            onError(0, error, response.response)
        } else {
            onSuccess(response.response!.statusCode, value as AnyObject?)
        }
        
    }
    
    func handlerResult(response : DataResponse<Any>, onSuccess: ServiceSuccessResponse, onError: ServiceErrorResponse) {
        
        let result = response.result;
        let error = result.error;
        let value = result.value;
        let statusCode = response.response?.statusCode
        
        if error != nil || !result.isSuccess {
            onError(statusCode == nil ? 0 : statusCode!, error, value as AnyObject?)
        } else {
            onSuccess(response.response!.statusCode, value as AnyObject?)
        }
        
    }
    
    
//    func downloadImage(imageView : UIImageView, url : String, placeholder : UIImage?, filter: ImageFilter? = nil){
//
//        let url = URL(string:url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!);
//
//        let placeholder = getImageWithColor(color: UIColor.gray, size: CGSize(width: 400, height: 200))
//
//        imageView.af_setImage(withURL: url!, placeholderImage: placeholder, filter: filter);
//    }
//
//    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
//        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
//        UIGraphicsBeginImageContextWithOptions(size, false, 0)
//        color.setFill()
//        UIRectFill(rect)
//        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
//        return image
//    }
    
    
}

