
import Foundation
import UIKit


final class DataStore {
    
    static let sharedInstance = DataStore()
    fileprivate init() {}
    
    var cards: [Cards] = []
    var images: [UIImage] = []
    
    func getCards(completion: @escaping () -> Void) {
        
        
        
        Networking.sharedInstance.getCards(onSuccess: { (code, data) in
            
                switch code {
                case 200:
                    
                    //print(data!)
                    DataStore.sharedInstance.cards = []
                    DataStore.sharedInstance.images = []
                    
                    
                    if let results = data as? [CardsJSON] {
                        for dict in results {
                            let newCard = Cards(dictionary: dict)
                            self.cards.append(newCard)
                        }
                        completion()
                    }
                    
                    //print(self.cards)
                    
                    
                default:
                    print("Error")
                    NotificationCenter.default.post(name: Notification.Name("ServerError"), object: nil)
                }
            }) { (code, error, data) in
                
                print("Other Error")
                NotificationCenter.default.post(name: Notification.Name("NetworkError"), object: nil)
            }
    }
    
    func getcardsImages(completion: @escaping () -> Void) {
        getCards {
            
            for card in self.cards {
                let url = URL(string: K.networking.baseImageURL + card.idName + ".png")
                
                print(url!)
                
                
                let data = try? Data(contentsOf: url!)
                if let imageData = data {
                    let image = UIImage(data: imageData)
                    self.images.append(image!)
                }
            }
            OperationQueue.main.addOperation {
                completion()
            }
        }
    }
    
    
}
