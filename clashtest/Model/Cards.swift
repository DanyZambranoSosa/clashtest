import Foundation

struct Cards {
    let name: String!
    let description: String!
    let idName: String!
    
    
    init(dictionary: CardsJSON) {
        self.name = dictionary["name"] as? String
        self.description = dictionary["description"] as? String
        self.idName = dictionary["idName"] as? String
    }
    
}
