//
//  CardsViewController.swift
//  clashtest
//
//  Created by Dany Zambrano on 1/19/19.
//  Copyright © 2019 hiberus. All rights reserved.
//

import UIKit

class CardsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UIGestureRecognizerDelegate  {
    
    @IBOutlet var collectionView: UICollectionView!
    
    let  store = DataStore.sharedInstance
    var  index: Int = 0
    var  sv: UIView?


    
    let notificationCenter = NotificationCenter.default
    let refreshControl = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ViewSettings()
        CollectionViewSettings()
        SetObservables()

        
        if store.cards.count == 0 {
           LoadData()
        }
        
    }
    
    
    
    //MARK: --- Settings ---
    func ViewSettings()
    {
        self.title = "Game Cards"
  
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        
    }
    
    
    func CollectionViewSettings()
    {
        let width = (view.frame.size.width) / 3
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: width)
    }
    
    func SetObservables()
    {
        notificationCenter.addObserver(self,
                                       selector: #selector(CardsViewController.ShowNetworkError),
                                       name: .NetworkError,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(CardsViewController.ShowServerError),
                                       name: .ServerError,
                                       object: nil)
    }
    
    
    
    
    
    //MARK: --- Functions ---
    func LoadData()
    {
        sv = UIViewController.displaySpinner(onView: self.view)
        
        store.getcardsImages {
            self.collectionView.reloadSections(IndexSet(integer: 0))
            UIViewController.removeSpinner(spinner: self.sv!)
        }
    }
    
    
    @objc func ShowNetworkError()
    {
        UIViewController.removeSpinner(spinner: sv!)
        
        presentAlertWithTitle(title: "Error Network", message: "Check your network and try again :)!", options: "Ok") { (option) in
            //print("option: \(option)")
            
            self.navigationController?.popViewController(animated: true)
            
        }
        
    }
    
    @objc func ShowServerError()
    {
        UIViewController.removeSpinner(spinner: sv!)
        
        presentAlertWithTitle(title: "Error Server", message: "Ups there is a problem :( /n Swip down and try again!", options: "Ok") { (option)in

            
        }
        
    }
    
    @objc private func refreshData(_ sender: Any)
    {
        LoadData()
        
        self.refreshControl.endRefreshing()
    }

    
    
    
    
    
    

    //MARK: --- Protocolos CollectionView ---
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return store.cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! CardsCollectionViewCell
        
        let card = store.cards[indexPath.row]
        
        cell.displayContent(image: store.images[indexPath.row], name: card.name)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print(indexPath.item)
        index = indexPath.item
        
        self.performSegue(withIdentifier: "segueCardSelected", sender: self)
    }
    
    


    //MARK: --- Segue PushData ---
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segueCardSelected") {
    
            let vc = segue.destination as! CardViewController
            vc.indice = index
        }
    }


}




//MARK: --- Extensions ---
extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}

extension Notification.Name {
    static let NetworkError = Notification.Name( rawValue: "NetworkError")
    static let ServerError = Notification.Name( rawValue: "ServerError")
}

extension UIViewController {
    
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
}

