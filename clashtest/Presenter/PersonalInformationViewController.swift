//
//  PersonalInformationViewController.swift
//  clashtest
//
//  Created by Dany Zambrano on 1/19/19.
//  Copyright © 2019 hiberus. All rights reserved.
//

import UIKit

class PersonalInformationViewController: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SetImageSettings()
    }
    
    
    //MARK: --- Settings ----
    func SetImageSettings()
    {
        imgProfile.layer.borderWidth = 1
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.borderColor = UIColor.black.cgColor
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        imgProfile.clipsToBounds = true
    }


}
