//
//  CardViewController.swift
//  clashtest
//
//  Created by Dany Zambrano on 1/19/19.
//  Copyright © 2019 hiberus. All rights reserved.
//

import UIKit

class CardViewController: UIViewController {

    
    @IBOutlet weak var lblDSescription: UILabel!
    @IBOutlet weak var imgCard: UIImageView!
    
    let  store = DataStore.sharedInstance
    
    var indice: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setData()
        
        print(indice)
    }
    
    
    
    //MARK: --- Setting ---
    func setData()
    {
        
        let card = store.cards[indice]
        
        //print(store.images[indice])
        //print(card.name)
        self.title = card.name
        lblDSescription.text = card.description
        imgCard.image =  store.images[indice]
    }
    

}
