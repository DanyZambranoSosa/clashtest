//
//  CardsCollectionViewCell.swift
//  clashtest
//
//  Created by Dany Zambrano on 1/19/19.
//  Copyright © 2019 hiberus. All rights reserved.
//

import UIKit

class CardsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemDescription: UILabel!
    
    func displayContent(image: UIImage, name: String) {
        itemImage.image = image
        itemDescription.text = name
    }
    
    
}
