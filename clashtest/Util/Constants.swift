//
//  Constants.swift
//  clashtest
//
//  Created by Dany Zambrano on 1/19/19.
//  Copyright © 2019 hiberus. All rights reserved.
//

import Foundation
import UIKit

struct K {
    
    
    struct segueId {
        static let cardSelected = "segueCardSelected"
    }
    
    struct networking {
        static let baseURL = "http://www.clashapi.xyz/"
        static let baseImageURL =  baseURL + "images/cards/"
    }
    
    struct error {
        static let error_500 = "Error de servidor interno. Por favor, inténtelo de nuevo más tarde"
        static let error_default = "Por favor, inténtelo de nuevo más tarde"
    }
    
    struct string {
        static let loading = "Loading Cards ..."
    }
    
    
}
